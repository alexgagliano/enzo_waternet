from matplotlib import pylab
import sys
import yt
from yt.mods import *
import numpy as np
import pandas as pd

do_fH2 = True
do_t_cool = True

dust = True
if dust:
    keyword = 'with_dust'
else:
    keyword = 'without_dust'

filename = sys.argv[1]
es = yt.simulation(filename, 'Enzo', find_outputs = True)
es.get_time_series()

T = []
nH = []
Z = []
fH2 = []
Tdust = []
#t_cool = []
t_dyn = []

for pf in es:
    #Find maximum density, and sample all points from there
    print(pf.current_time)
    ad = pf.all_data()
    dens = ad['enzo','Density']
    npd = np.array(dens)
    idx = np.argmax(npd)

    temp_temp = ad['enzo','Temperature']
    nH_temp = ad['enzo','HI_Density'] + ad['enzo','HII_Density'] \
    + ad['enzo','HM_Density'] + ad['enzo','H2I_Density'] + ad['enzo','H2II_Density'] + ad['enzo','HDI_Density']/2.
    print(nH_temp)
    metal_temp = ad['gas', 'metallicity']
    fH2_temp = ad['gas','H2_fraction']
    cool_temp = ad['enzo','Cooling_Time']
    dynT_temp = ad['gas','dynamical_time']

    temp_point = temp_temp[idx]
    nH_point = nH_temp[idx]
    metal_point = metal_temp[idx]
    fH2_point = fH2_temp[idx]
    cool_point = cool_temp[idx]
    dynT_point = dynT_temp[idx]

    T.append(temp_point)
    nH.append(nH_point)
    Z.append(metal_point)
    if do_fH2: fH2.append(fH2_point)
    t_dyn.append(dynT_point)
    del pf

T = np.array(T)
nH = np.array(nH)
Z = np.array(Z)
fH2 = np.array(fH2)
Tdust = np.array(Tdust)
t_dyn = np.array(t_dyn)

colors = ["black", "purple", "blue", "green", "orange", "red"]
met = np.round(np.log10(Z))
i = 0

T_df = pd.DataFrame({"log(nH)" : np.log10(nH), "T" : T})
logfH2_df = pd.DataFrame({"log(nH)" : np.log10(nH), "log(fH2)" : np.log10(fH2)})

T_df.to_csv("T_v_lognH_[Z]=%d_2005.csv" % met[i], index=None)

if do_fH2:
	logfH2_df.to_csv("logfH2_v_lognH_[Z]=%d_2005.csv" % met[i], index=None)
