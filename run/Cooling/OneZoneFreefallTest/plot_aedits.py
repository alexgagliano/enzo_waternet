from matplotlib import pylab
import sys
import yt
from yt.mods import *
import numpy as np

do_fH2 = False
do_t_cool = True

dust = True
if dust:
    keyword = 'with_dust'
else:
    keyword = 'without_dust'

filename = sys.argv[1]
es = yt.simulation(filename, 'Enzo', find_outputs = True)
es.get_time_series()

T = []
nH = []
Z = []
dens = []
fH2 = []
Tdust = []
#t_cool = []
t_dyn = []

for pf in es:
#    point_obj = pf.point([3,3,0])
#    temp_point = point_obj['enzo', 'Temperature']
#    nH_point = point_obj['enzo', 'HI_Density'] + point_obj['enzo', 'HII_Density'] + point_obj['enzo', 'HM_Density'] 
#    + point_obj['enzo', 'H2I_Density'] + point_obj['enzo', 'H2I_Density'] + point_obj['enzo', 'HDI_Density']/2.
#    metal_point = point_obj['gas', 'metallicity']
#    fH2_point = point_obj['gas','H2_fraction']
#    cool_point = point_obj['enzo','Cooling_Time']
#    dynT_point = point_obj['gas','dynamical_time']

    #Find maximum density, and sample all points from there
    print(pf.current_time)
    ad = pf.all_data()
    dens = ad['enzo','Density']
    npd = np.array(dens)
    idx = np.argmax(npd) 

    temp_temp = ad['enzo','Temperature']
    nH_temp = ad['enzo','HI_Density'] + ad['enzo','HII_Density'] \
    + ad['enzo','HM_Density'] + ad['enzo','H2I_Density'] + ad['enzo','H2II_Density'] + ad['enzo','HDI_Density']/2.
    print(nH_temp)
    metal_temp = ad['gas', 'metallicity']
    fH2_temp = ad['gas','H2_fraction']
    cool_temp = ad['enzo','Cooling_Time']
    dynT_temp = ad['gas','dynamical_time']   

    temp_point = temp_temp[idx]
    nH_point = nH_temp[idx]
    metal_point = metal_temp[idx]
    fH2_point = fH2_temp[idx]
    cool_point = cool_temp[idx]
    dynT_point = dynT_temp[idx]
 
    T.append(temp_point)
    nH.append(nH_point)
    Z.append(metal_point)
    if do_fH2: fH2.append(fH2_point)
    t_dyn.append(dynT_point)
    del pf

T = np.array(T)
nH = np.array(nH)
Z = np.array(Z)
fH2 = np.array(fH2)
Tdust = np.array(Tdust)
t_dyn = np.array(t_dyn)

colors = ["black", "purple", "blue", "green", "orange", "red"]

#rounding metals for labeling
met = np.round(np.log10(Z))
i = 0
for i in range(1):
	pylab.semilogy(np.log10(nH), T, 
                 label='log (Z/Z$_{\odot}$) = %d' % met[i],
                 color=colors[i], linestyle='-')
pylab.xlim(xmin=-2, xmax=10)
pylab.ylim(ymin = 1.e0, ymax=1.e5)
pylab.xlabel('log(n [cm$^{-3}$])')
pylab.ylabel('T [K]')
pylab.legend(labelspacing=0.0, loc='lower right')
pylab.savefig('Enzo_T_vs_logn_%s.png' % keyword)
pylab.clf()

if do_fH2:
    pylab.loglog(n, fH2, 
                     label='log (Z/Z$_{\odot}$) = %d' % met[i],
                     color=colors[i])
    pylab.xlim(xmin=1.0)
    pylab.xlabel('n [cm$^{-3}$]')
    pylab.ylabel('f$_{H_{2}}$')
#    pylab.ylim(1e-5, 1)
    pylab.legend(labelspacing=0.0, loc='lower right')
    pylab.savefig('Enzo_fH2_vs_logn.png')
    pylab.clf()
