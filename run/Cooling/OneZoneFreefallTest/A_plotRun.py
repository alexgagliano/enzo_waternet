from matplotlib import pylab
import sys
import yt
from yt.mods import *
import numpy as np
import pandas as pd

do_fH2 = True
do_t_cool = True

dust = True

if dust:
    keyword = 'with_dust'
else:
    keyword = 'without_dust'

logZ = [-6., -4., -2., 0.]
for i in range(len(logZ)):
	filenameT = 'T_v_lognH_[Z]=%d_2005.csv' % logZ[i]

	T = []
	n = []

	with open(filenameT, 'r') as dataT:
		lines = dataT.readlines()
		for line in lines[1:]:
			p = line.split(',')
			n.append(float(p[1]))
			T.append(float(p[0]))

	colors = ["blue", "brown", "magenta", "purple"]
	
	pylab.semilogy(n, T, label='log (Z/Z$_{\odot}$) = %d, Sim' % logZ[i], color=colors[i], linestyle='-')

	if i == 0:
		pylab.xlim(xmin = -1.5, xmax=10)
		pylab.ylim(ymin = 1e0, ymax=1e5)
		pylab.xlabel('log(nH [cm$^{-3}$])')
		pylab.ylabel('T [K]')

	###OPEN OMUKAI 2000 FOR COMPARISON
	#with open('logT_v_logn_Z=%d_Omukai.csv' % logZ[i], 'r') as omukaiData:
	
	## OPEN OMUKAI 2005 FOR COMPARISON
	with open('T_vs_logn__Z=%d_WaterOmukai2005.csv' % logZ[i], 'r') as omukaiData:	
		lines = omukaiData.readlines()
		Omukai_n = []
		Omukai_T = []
		for line in lines[6:]:
			p = line.split(',')
			Omukai_n.append(float(p[0]))
			Omukai_T.append(float(p[1]))	
		pylab.semilogy(Omukai_n, Omukai_T, label='log (Z/Z$_{\odot}$) = %d, Omukai' % logZ[i], color=colors[i], linestyle=':')

		Omukai_n = []
		Omukai_T = []

leg = pylab.legend(fancybox = True, labelspacing=0.0, loc='best')
leg.get_frame().set_alpha(0.5)
pylab.savefig('T_vs_nH_%s_2005.png' % keyword)
pylab.clf()

#TAKE TWO : fH2!!!
if do_fH2:
	for i in range(len(logZ)):
		filenamefH2 = 'logfH2_v_lognH_[Z]=%d_2005.csv' % logZ[i]
		n = []
		fH2 = []
		with open(filenamefH2, 'r') as datafH2:
			lines = datafH2.readlines()
			for line in lines[1:]:
				p = line.split(',')
				n.append(float(p[1]))
				fH2.append(float(p[0]))		
		pylab.plot(n, fH2, label='log (Z/Z$_{\odot}$) = %d' % logZ[i], color=colors[i])
		if i == 0:
			pylab.xlim(xmin = -1.5, xmax=10)
			pylab.xlabel('number density log nH (cm$^{-3}$)')
			pylab.ylabel('H$_2$ fraction log f$_{H_{2}}$')

		## OPEN OMUKAI 2005 FOR COMPARISON
		with open('logfH2_vs_logn_[ZH]=%d_Omukai2005.csv' % logZ[i], 'r') as omukaiData:
			lines = omukaiData.readlines()
			Omukai_n = []
			Omukai_fH2 = []
			for line in lines[6:]:
				p = line.split(',')
				Omukai_n.append(float(p[0]))
				Omukai_fH2.append(float(p[1]))
			pylab.plot(Omukai_n, Omukai_fH2, label='log (Z/Z$_{\odot}$) = %d, Omukai' % logZ[i], color=colors[i], linestyle=':')

			Omukai_n = []
			Omukai_fH2 = []

	leg = pylab.legend(fancybox = True, labelspacing=0.0, loc='lower right')
	leg.get_frame().set_alpha(0.5)
	pylab.savefig('logfH2_vs_lognH_%s_2005.png' % keyword)
	pylab.clf()

