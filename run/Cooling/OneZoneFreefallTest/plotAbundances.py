########################################################################
#
# Free-fall example script
#
#
# Copyright (c) 2013-2016, Grackle Development Team.
#
# Distributed under the terms of the Enzo Public Licence.
#
# The full license is in the file LICENSE, distributed with this
# software.
########################################################################

from matplotlib import pyplot
import os
import yt
from yt.mods import *
import numpy as np
import pandas as pd
import sys

## SANDBOX

filename = sys.argv[1]
es = yt.simulation(filename, 'Enzo', find_outputs = True)
es.get_time_series()

nH = []
Z = []

de = []
H2I = []
H2II = []
HDI = []
OI = []
OII = []
CI = []
CII = []
OHI = []
OHII = []
O2I = []
O2II = []
H2OI = []
H2OII = []
H3OII = []
CHI = []
CH2I = []
CH3I = []
CH4I = []
COI = []
COII = []
CO2I = []

for pf in es:

    ad = pf.all_data()
    dens = ad['enzo','Density']
    npd = np.array(dens)
    idx = np.argmax(npd)

    nHdens_temp = ad['enzo','HI_Density'] + ad['enzo','HII_Density'] \
    + ad['enzo','HM_Density'] + ad['enzo','H2I_Density'] + ad['enzo','H2II_Density'] + ad['enzo','HDI_Density']/2.
    metal_temp = ad['gas', 'metallicity']

    de_temp = ad['enzo', 'Electron_Density']
    H2I_temp = ad['enzo', 'H2I_Density']
    H2II_temp = ad['enzo', 'H2II_Density']
    HDI_temp = ad['enzo', 'HDI_Density']
    OI_temp = ad['enzo','OI_Density'] 
    OII_temp = ad['enzo','OII_Density'] 
    CI_temp = ad['enzo','CI_Density'] 
    CII_temp = ad['enzo','CII_Density']  
    OHI_temp = ad['enzo','OHI_Density'] 
    OHII_temp = ad['enzo','OHII_Density']
    O2I_temp = ad['enzo','O2I_Density'] 
    O2II_temp = ad['enzo','O2II_Density']  
    H2OI_temp = ad['enzo','H2OI_Density'] 
    H2OII_temp = ad['enzo','H2OII_Density']
    H3OII_temp = ad['enzo','H3OII_Density'] 
    CHI_temp = ad['enzo','CHI_Density'] 
    CH2I_temp = ad['enzo','CH2I_Density'] 
    CH3I_temp = ad['enzo','CH3I_Density'] 
    CH4I_temp = ad['enzo','CH4I_Density'] 
    COI_temp = ad['enzo','COI_Density'] 
    COII_temp = ad['enzo','COII_Density'] 
    CO2I_temp = ad['enzo','CO2I_Density'] 

    nHdens_point = nHdens_temp[idx]
    metal_point = metal_temp[idx]
    de_point = de_temp[idx]
    H2I_point = H2I_temp[idx]
    H2II_point = H2II_temp[idx]
    HDI_point = HDI_temp[idx]
    OI_point = OI_temp[idx] 
    OII_point = OII_temp[idx] 
    CI_point = CI_temp[idx] 
    CII_point = CII_temp[idx]
    OHI_point = OHI_temp[idx]
    OHII_point = OHII_temp[idx]
    O2I_point = O2I_temp[idx]
    O2II_point = O2II_temp[idx]
    H2OI_point = H2OI_temp[idx]
    H2OII_point = H2OII_temp[idx]
    H3OII_point = H3OII_temp[idx]
    CHI_point = CHI_temp[idx]
    CH2I_point = CH2I_temp[idx]
    CH3I_point = CH3I_temp[idx]
    CH4I_point = CH4I_temp[idx]
    COI_point = COI_temp[idx]
    COII_point = COII_temp[idx]
    CO2I_point = CO2I_temp[idx]

    nH.append(nHdens_point)
    Z.append(metal_point)
    de.append(de_point)
    H2I.append(H2I_point)
    H2II.append(H2II_point)
    HDI.append(HDI_point)
    OI.append(OI_point)
    OII.append(OII_point)
    CI.append(CI_point)
    CII.append(CII_point)
    OHI.append(OHI_point)
    OHII.append(OHII_point)
    O2I.append(O2I_point)
    O2II.append(O2II_point)
    H2OI.append(H2OI_point)
    H2OII.append(H2OII_point)
    H3OII.append(H3OII_point)
    CHI.append(CHI_point)
    CH2I.append(CH2I_point)
    CH3I.append(CH3I_point)
    CH4I.append(CH4I_point)
    COI.append(COI_point)
    COII.append(COII_point)
    CO2I.append(CO2I_point)

    del pf

nH = np.array(nH)
Z = np.array(Z)

de = np.array(de)
H2I = np.array(H2I)
H2II = np.array(H2II)
HDI = np.array(HDI)
OI = np.array(OI)
OII = np.array(OII)
CI = np.array(CI)
CII = np.array(CII)
OHI = np.array(OHI)
OHII = np.array(OHII)
O2I = np.array(O2I)
O2II = np.array(O2II)
H2OI = np.array(H2OI)
H2OII = np.array(H2OII)
H3OII = np.array(H3OII)
CHI = np.array(CHI)
CH2I = np.array(CH2I)
CH3I = np.array(CH3I)
CH4I = np.array(CH4I)
COI = np.array(COI)
COII = np.array(COII)
CO2I = np.array(CO2I)

colors = ["black", "purple", "blue", "green", "orange", "red"]
met = np.round(np.log10(Z))
##

if __name__=="__main__":
    # Set solver parameters
    colors = ["blue", "brown", "magenta", "purple"]
    #metallicity = [1.e-6, 1.e-4, 1.e-2, 1.e-0]
    #logZ = [-6, -4, -2, 0]
    metallicity = [1.e-6]
    logZ = [-6]
    for i in range(len(metallicity)):

        if logZ[i] != met[0]:
           print("Sorry! Plots can only be made for [Z/Z_sun] = -6, as it's the only one of Omukai's abundance plots I digitized.")
           sys.exit()

        # FIRST SET OF PLOTS: H2O, OH, O
        pyplot.plot(np.log10(nH), np.log10((H2OI + H2OII) / (nH)) , label ='H2O', color=colors[0])
        pyplot.plot(np.log10(nH), np.log10((OHI + OHII) / (nH)) , label ='OH', color=colors[1])
        pyplot.plot(np.log10(nH), np.log10((OI + OII) / (nH)) , label ='O', color=colors[2])

       #pyplot.plot(np.log10( nH), np.log10((data["O2_density"] + data["O2plus_density"]) / (nH)), label = 'O2', color=colors[3])


        # OPEN OMUKAI 2005 FOR COMPARISON
        with open('logy(H2O)_v_logn_Z=%d_Omukai2005.csv' % logZ[i], 'r') as omukaiData:
                lines = omukaiData.readlines()
                Omukai_logn = []
                Omukai_spec = []
                for line in lines[6:]:
                        p = line.split(',')
                        Omukai_logn.append(float(p[0]))
                        Omukai_spec.append(float(p[1]))
                pyplot.plot(Omukai_logn, Omukai_spec, label='H2O, Omukai', color=colors[0], linestyle=':')

                Omukai_logn = []
                Omukai_spec = []

        ## OPEN OMUKAI 2005 FOR COMPARISON
        with open('logy(OH)_v_logn_Z=%d_Omukai2005.csv' % logZ[i], 'r') as omukaiData:
                lines = omukaiData.readlines()
                Omukai_logn = []
                Omukai_spec = []
                for line in lines[6:]:
                        p = line.split(',')
                        Omukai_logn.append(float(p[0]))
                        Omukai_spec.append(float(p[1]))
                pyplot.plot(Omukai_logn, Omukai_spec, label='OH, Omukai', color=colors[1], linestyle=':')

                Omukai_logn = []
                Omukai_spec = []

        ## OPEN OMUKAI 2005 FOR COMPARISON
        with open('logy(O)_v_logn_Z=%d_Omukai2005.csv' % logZ[i], 'r') as omukaiData:
                lines = omukaiData.readlines()
                Omukai_logn = []
                Omukai_spec = []
                for line in lines[6:]:
                        p = line.split(',')
                        Omukai_logn.append(float(p[0]))
                        Omukai_spec.append(float(p[1]))
                pyplot.plot(Omukai_logn, Omukai_spec, label='O, Omukai', color=colors[2], linestyle=':')

                Omukai_logn = []
                Omukai_spec = []
 
        pyplot.xlabel("log($n_H$ [cm$^{-3}$])")
        pyplot.ylabel("log y(O, OH, H2O)")

        #pyplot.ylim(ymin = -15, ymax = -5)
        pyplot.xlim(xmin = -1.5, xmax = 12)
        leg = pyplot.legend(fancybox = True, labelspacing=0.0, loc='best')
        leg.get_frame().set_alpha(0.5)
 
        pyplot.savefig("Water_abundances1_%d.png" % (logZ[i]))
        pyplot.clf()
      
        # SECOND SET OF PLOTS: H2, e, HD 
        pyplot.plot(np.log10( nH), np.log10((H2I + H2II) / (nH)) , label ='H2', color=colors[0])
        #pyplot.plot(np.log10( nH), np.log10(data["de"] / (mass_electron_cgs / mass_hydrogen_cgs * nH)) , label ='e', color=colors[1])
        pyplot.plot(np.log10( nH), np.log10(de / (nH)) , label ='e', color=colors[1])

        pyplot.plot(np.log10( nH), np.log10(HDI / (nH)) , label ='HD', color=colors[2])

        
        ## OPEN OMUKAI 2005 FOR COMPARISON
        with open('logy(H2)_v_logn_Z=%d_Omukai2005.csv' % logZ[i], 'r') as omukaiData:
                lines = omukaiData.readlines()
                Omukai_logn = []
                Omukai_spec = []
                for line in lines[6:]:
                        p = line.split(',')
                        Omukai_logn.append(float(p[0]))
                        Omukai_spec.append(float(p[1]))
                pyplot.plot(Omukai_logn, Omukai_spec, label='H2, Omukai', color=colors[0], linestyle=':')

                Omukai_logn = []
                Omukai_spec = []

        ## OPEN OMUKAI 2005 FOR COMPARISON
        with open('logy(e)_v_logn_Z=%d_Omukai2005.csv' % logZ[i], 'r') as omukaiData:
                lines = omukaiData.readlines()
                Omukai_logn = []
                Omukai_spec = []
                for line in lines[6:]:
                        p = line.split(',')
                        Omukai_logn.append(float(p[0]))
                        Omukai_spec.append(float(p[1]))
                pyplot.plot(Omukai_logn, Omukai_spec, label='de, Omukai', color=colors[1], linestyle=':')

                Omukai_logn = []
                Omukai_spec = []

        ## OPEN OMUKAI 2005 FOR COMPARISON
        with open('logy(HD)_v_logn_Z=%d_Omukai2005.csv' % logZ[i], 'r') as omukaiData:
                lines = omukaiData.readlines()
                Omukai_logn = []
                Omukai_spec = []
                for line in lines[6:]:
                        p = line.split(',')
                        Omukai_logn.append(float(p[0]))
                        Omukai_spec.append(float(p[1]))
                pyplot.plot(Omukai_logn, Omukai_spec, label='HD, Omukai', color=colors[2], linestyle=':')

                Omukai_logn = []
                Omukai_spec = []


        pyplot.xlabel("log($n_H$ [cm$^{-3}$])")
        pyplot.ylabel("log y(H2, e, HD)")

        #pyplot.ylim(ymin = -15)
        pyplot.xlim(xmin = -1.5, xmax = 12)
        leg = pyplot.legend(fancybox = True, labelspacing=0.0, loc='best')
        leg.get_frame().set_alpha(0.5)

        pyplot.savefig("Water_abundances2_%d.png" % (logZ[i]))
        pyplot.clf()

        # THIRD SET OF PLOTS: C+, C, CO
        pyplot.plot(np.log10(nH), np.log10(CII / (nH)) , label ='C+', color=colors[0])
        pyplot.plot(np.log10(nH), np.log10(CI / (nH)) , label ='C', color=colors[1])
        pyplot.plot(np.log10(nH), np.log10((COI + COII) / (nH)) , label ='CO', color=colors[2])
        #pyplot.plot(np.log10(nH), np.log10(CO2I + CO2II) / (nH)) , label ='CO2', color=colors[3])

        
        ## OPEN OMUKAI 2005 FOR COMPARISON
        with open('logy(C+)_v_logn_Z=%d_Omukai2005.csv' % logZ[i], 'r') as omukaiData:
                lines = omukaiData.readlines()
                Omukai_logn = []
                Omukai_spec = []
                for line in lines[6:]:
                        p = line.split(',')
                        Omukai_logn.append(float(p[0]))
                        Omukai_spec.append(float(p[1]))
                pyplot.plot(Omukai_logn, Omukai_spec, label='C+, Omukai', color=colors[0], linestyle=':')

                Omukai_logn = []
                Omukai_spec = []

        ## OPEN OMUKAI 2005 FOR COMPARISON
        with open('logy(C)_v_logn_Z=%d_Omukai2005.csv' % logZ[i], 'r') as omukaiData:
                lines = omukaiData.readlines()
                Omukai_logn = []
                Omukai_spec = []
                for line in lines[6:]:
                        p = line.split(',')
                        Omukai_logn.append(float(p[0]))
                        Omukai_spec.append(float(p[1]))
                pyplot.plot(Omukai_logn, Omukai_spec, label='C, Omukai', color=colors[1], linestyle=':')

                Omukai_logn = []
                Omukai_spec = []

        ## OPEN OMUKAI 2005 FOR COMPARISON
        with open('logy(CO)_v_logn_Z=%d_Omukai2005.csv' % logZ[i], 'r') as omukaiData:
                lines = omukaiData.readlines()
                Omukai_logn = []
                Omukai_spec = []
                for line in lines[6:]:
                        p = line.split(',')
                        Omukai_logn.append(float(p[0]))
                        Omukai_spec.append(float(p[1]))
                pyplot.plot(Omukai_logn, Omukai_spec, label='CO, Omukai', color=colors[2], linestyle=':')

                Omukai_logn = []
                Omukai_spec = []


        pyplot.xlabel("log($n_H$ [cm$^{-3}$])")
        pyplot.ylabel("log y(C+, C, CO)")

        #pyplot.ylim(ymin = -20, ymax = -8)
        #pyplot.ylim(ymin = -15, ymax = -6)
        pyplot.xlim(xmin = -1.5, xmax = 12)
        leg = pyplot.legend(fancybox = True, labelspacing=0.0, loc='best')
        leg.get_frame().set_alpha(0.5)
        pyplot.savefig("Water_abundances3_%d.png" % (logZ[i]))
        pyplot.clf()

